﻿using WordleSolverWPF.String;

namespace WordleSolverWPF.Logic;

internal static class Simulation
{
    private static float Remap(this float value, float from1, float to1, float from2, float to2) => (value - from1) / (to1 - from1) * (to2 - from2) + from2;

    internal static KeyValuePair<List<string>, float> RunWeightedWordSimulation(string[] validWords, CancellationToken cancelCalculationToken) => RunWeightedWordSimulation(validWords, validWords, cancelCalculationToken);

    internal static KeyValuePair<List<string>, float> RunWeightedWordSimulation(string[] wordset, string[] validWords, CancellationToken cancelCalculationToken)
    {
        var @lock = new object();
        float bestWeight = 0;
        var bestWords = new List<string>() { wordset[0] };

        wordset.AsParallel().ForAll(startWord =>
        {
            if (cancelCalculationToken.IsCancellationRequested)
            {
                return;
            }

            var wordWeight = EvaluateStartWordBasedOnWeight(startWord, validWords);

            lock (@lock)
            {
                if (wordWeight == bestWeight)
                {
                    bestWords.Add(startWord);
                }
                else if (wordWeight > bestWeight)
                {
                    bestWords = new List<string> { startWord };
                    bestWeight = wordWeight;
                }
            }
        });

        return new KeyValuePair<List<string>, float>(bestWords, bestWeight);
    }

    private static float EvaluateStartWordBasedOnWeight(string startWord, string[] validWords)
    {
        float weight = 0;
        var LetterValues = new float[5];

        for (var i = 0; i < validWords.Length; i++)
        {
            GetWordValue(startWord, validWords[i], ref LetterValues);

            for (var j = 0; j < LetterValues.Length; j++)
            {
                var characterWeight = CharacterWeight.Weights[j][startWord[j] - 97].Remap(0, 1, 0.6f, 1);
                weight += LetterValues[j] * characterWeight;
            }
        }

        return weight / (startWord.DuplicateCount() + 1);
    }

    private static void GetWordValue(string guess, string actualWord, ref float[] states)
    {
        for (var i = 0; i < states.Length; i++)
        {
            if (guess[i] == actualWord[i])
            {
                states[i] = 5f;
            }
            else if (actualWord.Contains(guess[i]))
            {
                states[i] = 2f;
            }
            else
            {
                states[i] = 1f;
            }
        }
    }
}
