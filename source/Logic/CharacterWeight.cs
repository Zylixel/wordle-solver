﻿using WordleSolver.DataProvider;

namespace WordleSolverWPF.Logic;

internal static class CharacterWeight
{
    internal static float[][] Weights = new float[5][];

    static CharacterWeight()
    {
        CalculateCharacterWeights(WordListProvider.GetAnswers());
    }

    private static void CalculateCharacterWeights(IList<string> words)
    {
        for (var i = 0; i < Weights.Length; i++)
        {
            Weights[i] = new float[26];

            for (var k = 0; k < words.Count; k++)
            {
                Weights[i][words[k][i] - 97]++;
            }
        }

        for (var i = 0; i < Weights.Length; i++)
        {
            var columnMax = Weights[i].Max();

            for (var j = 0; j < Weights[i].Length; j++)
            {
                Weights[i][j] /= columnMax;
            }
        }
    }
}
