﻿using System.Diagnostics;
using WordleSolver.DataProvider;
using WordleSolverWPF.Logic;
using WordleSolverWPF.String;

namespace WordleSolver.Logic;

internal static class Algorithm
{
    internal static string[] ValidGuesses(
        IList<string> wordList,
        IList<char>[] invalidCharacters,
        IList<char> validCharacters,
        char?[] correctCharacters)
    {
        var ret = new List<string>();

        for (var i = 0; i < wordList.Count; i++)
        {
            var word = wordList[i];

            if (!word.ContainsCorrectCharacters(correctCharacters) || word.ContainsInvalidCharacters(invalidCharacters))
            {
                continue;
            }

            if (validCharacters.Count != 0)
            {
                if (word.ContainsAllCharacters(validCharacters))
                {
                    ret.Add(word);
                }
            }
            else
            {
                ret.Add(word);
            }
        }

        return ret.ToArray();
    }

    internal static string[] NarrowDownGuesses(
        IList<string> wordList,
        IList<char>[] invalidCharacters,
        IList<char?> correctCharacters)
    {
        var ret = new List<string>();
        var correctCharactersNonNull = correctCharacters.Where(x => x.HasValue).Select(x => x!.Value).ToArray();

        for (var i = 0; i < wordList.Count; i++)
        {
            var word = wordList[i];

            if (!word.ContainsInvalidCharacters(invalidCharacters) && !word.ContainsAnyCharacters(correctCharactersNonNull))
            {
                ret.Add(word);
            }
        }

        return ret.ToArray();
    }

    internal static KeyValuePair<string, int> CalculateBestGuess(
        List<char>[] invalidCharacters,
        List<char> validCharacters,
        char?[] correctCharacters,
        int guessesCount,
        CancellationToken cancelCalculationToken)
    {
        var validGuessList = WordListProvider.GetValidGuesses();
        var answerList = WordListProvider.GetAnswers();
        var validAnswers = ValidGuesses(answerList, invalidCharacters, validCharacters, correctCharacters);

        // Could mean that the answer set is different, grab all valid guesses and assume they are the answers
        if (validAnswers.Length == 0)
        {
            validAnswers = ValidGuesses(validGuessList, invalidCharacters, validCharacters, correctCharacters);
        }

        if (guessesCount > 1 && validAnswers.Length > guessesCount)
        {
            string[]? invalidWords;
            var i = 0;
            do
            {
                invalidWords = NarrowDownGuesses(validGuessList, invalidCharacters, correctCharacters);

                // If theres no way to narrow down the guesses, then remove the invalid vowels to make way for words
                if (invalidWords.Length == 0)
                {
                    List<char>? x;

                    do
                    {
                        x = invalidCharacters[i % invalidCharacters.Length];
                        i++;
                    }
                    while (x is null || x.Count == 0);

                    invalidCharacters[i % invalidCharacters.Length].RemoveAll(x => "aeiouy".Contains(x));
                }
            }
            while (invalidWords.Length == 0);

            var bestGuess = Simulation.RunWeightedWordSimulation(invalidWords, validAnswers, cancelCalculationToken);

            Debug.Assert(bestGuess.Key.Count > 0);

            return new KeyValuePair<string, int>(bestGuess.Key[0], validAnswers.Length);

        }
        else if (validAnswers.Length > 1)
        {
            return new KeyValuePair<string, int>(Simulation.RunWeightedWordSimulation(validAnswers, cancelCalculationToken).Key[0], validAnswers.Length);
        }
        else if (validAnswers.Length == 1)
        {
            return new KeyValuePair<string, int>(validAnswers[0], 1);
        }
        else
        {
            return new KeyValuePair<string, int>(string.Empty, 0);
        }
    }
}
