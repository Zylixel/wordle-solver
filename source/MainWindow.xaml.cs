﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using WordleSolver.Logic;
using WordleSolverWPF.Board;
using WordleSolverWPF.Cache;
using WordleSolverWPF.WPFExtensions;

namespace WordleSolverWPF;

public partial class MainWindow : Window
{
    private readonly Image winImage;
    private readonly LetterButton[] buttons = new LetterButton[30];
    private int buttonIndex = 0;

    private bool guessWords = true;
    private KeyValuePair<string, int> BestGuess = new(string.Empty, 0);
    private readonly object guessLock = new();
    private CancellationTokenSource? currentCalculation;

    private string renderedGuess = "NEEDSUPDATE";

    public MainWindow()
    {
        InitializeComponent();
        winImage = (Image)FindName("WinImage");
        EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyDownEvent, new KeyEventHandler(AllKeyDown), true);
    }

    private void AllKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
        {
            if (BestGuess.Key.Length < 4)
            {
                return;
            }

            var row = GetPreviewRow();

            guessWords = false;
            for (var i = row; i < row + 5; i++)
            {
                SetButton(i, BestGuess.Key[i % 5]);
            }
            guessWords = true;
            UpdateBestGuess();

            return;
        }

        if (e.Key == Key.Escape)
        {
            guessWords = false;

            for (var i = 0; i < 5; i++)
            {
                buttons[i].State = ButtonState.State.InvalidLetter;
            }

            for (var i = 5; i < buttons.Length; i++)
            {
                buttons[i].Character = null;
            }

            renderedGuess = string.Empty;
            guessWords = true;
            buttonIndex = 5;

            UpdateBestGuess();
        }

        var numberKey = e.Key - Key.D0;
        if (numberKey >= 0 && numberKey <= 5)
        {
            for (var i = buttonIndex - 1; i >= 0; i--)
            {
                var column = i % 5;

                if (column == numberKey - 1)
                {
                    buttons[i].FakeClick();
                    return;
                }
            }
        }

        if (buttonIndex > 0 && e.Key == Key.Back)
        {
            buttonIndex--;
            var button = buttons[buttonIndex];
            button.Character = null;
        }
        else if (buttonIndex < buttons.Length)
        {
            if ((int)e.Key >= 44 && (int)e.Key <= 69)
            {
                SetButton(buttonIndex, e.Key.ToString()[0]);
            }
        }
    }

    private int GetPreviewRow()
    {
        var row = (int)Math.Ceiling(buttonIndex / 5f);
        row *= 5;
        return row;
    }

    private void SetButton(int index, char? value)
    {
        var correctCharacters = GetCorrectCharacters();
        var validCharacters = GetValidCharacters();

        var button = buttons[index];
        buttonIndex = index + 1;

        button.Character = value;

        if (button.Character is not null)
        {
            if (button.Character == correctCharacters[index % 5])
            {
                button.State = ButtonState.State.CorrectLetter;
            }
            else if (validCharacters.Contains(button.Character.Value))
            {
                button.State = ButtonState.State.ValidLetter;
            }
            else
            {
                button.State = ButtonState.State.InvalidLetter;
            }
        }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
        var i = 0;
        foreach (var button in this.FindVisualChildren<Button>())
        {
            if (i < 30)
            {
                buttons[i] = new LetterButton(button, true);
                buttons[i].Updated += LetterButtonUpdated;
            }
            i++;
        }

        CompositionTarget.Rendering += Render;

        UpdateBestGuess();
    }

    private void LetterButtonUpdated(object? sender, EventArgs e) => UpdateBestGuess();

    private void UpdateBestGuess()
    {
        if (!guessWords)
        {
            return;
        }

        if (currentCalculation is not null)
        {
            currentCalculation.Cancel();
            currentCalculation = null;
        }

        if (buttonIndex % 5 != 0)
        {
            BestGuess = new KeyValuePair<string, int>(string.Empty, 0);
            return;
        }

        var boardState = GetBoardState();

        if (BestGuessCache.TryGetGuess(boardState, out BestGuess))
        {
            return;
        }

        var calculation = new CancellationTokenSource();
        currentCalculation = calculation;
        BestGuess = new KeyValuePair<string, int>(string.Empty, 0);

        var invalidCharacters = GetInvalidCharacters();
        var validCharacters = GetValidCharacters();
        var correctCharacters = GetCorrectCharacters();
        var guessesLeft = GetGuessesLeft();

        ThreadPool.QueueUserWorkItem((_) =>
        {
            var bestGuess = Algorithm.CalculateBestGuess(invalidCharacters, validCharacters, correctCharacters, GetGuessesLeft(), calculation.Token);

            if (!calculation.IsCancellationRequested)
            {
                lock (guessLock)
                {
                    BestGuess = bestGuess;
                    BestGuessCache.CacheGuess(boardState, bestGuess);

                }
            }
        });
    }

    private int GetGuessesLeft() => 6 - (buttonIndex + 1) / 5;


    private void Render(object? sender, EventArgs e)
    {
        if (buttons is null)
        {
            return;
        }

        for (var i = 0; i < buttons.Length; i++)
        {
            buttons[i].ShowWinningAnimation = false;
        }

        var winning = winImage.Visibility == Visibility.Visible;
        var previewRow = GetPreviewRow();
        for (var i = previewRow; i < previewRow + 5; i++)
        {
            if (i >= buttons.Length)
            {
                break;
            }

            buttons[i].ShowWinningAnimation = winning;
        }

        for (var i = 0; i < buttons.Length; i++)
        {
            buttons[i].Render();
        }

        RenderBestGuess();
    }

    private void RenderBestGuess()
    {
        if (renderedGuess == BestGuess.Key)
        {
            return;
        }

        winImage.Visibility = Visibility.Hidden;

        if (BestGuess.Key == string.Empty)
        {
            SetAnswer(BestGuess.Key, ButtonState.State.Preview);
        }
        else if (BestGuess.Value == 0)
        {
            SetAnswer(BestGuess.Key, ButtonState.State.Preview);
        }
        else if (BestGuess.Value == 1)
        {
            SetAnswer(BestGuess.Key, ButtonState.State.PreviewCorrect);
            winImage.Visibility = Visibility.Visible;
        }
        else
        {
            if (BestGuess.Value <= GetGuessesLeft())
            {
                winImage.Visibility = Visibility.Visible;
            }

            SetAnswer(BestGuess.Key, ButtonState.State.Preview);
        }

        renderedGuess = BestGuess.Key;
    }

    private void SetAnswer(string answer, ButtonState.State buttonState)
    {
        var row = GetPreviewRow();
        guessWords = false;

        for (var i = 0; i < buttons.Length; i++)
        {
            if (buttons[i].State == ButtonState.State.Preview)
            {
                buttons[i].Character = null;
            }
        }

        if (answer.Length < 4)
        {
            for (var i = row; i < row + 5; i++)
            {
                buttons[i].Character = null;
            }
        }
        else
        {
            for (var i = row; i < row + 5; i++)
            {
                if (i >= buttons.Length)
                {
                    break;
                }

                buttons[i].Character = answer[i % 5];
                buttons[i].State = buttonState;
            }
        }
        guessWords = true;
    }

    private List<char>[] GetInvalidCharacters()
    {
        var invalidCharacters = new List<char>[]
        {
            new List<char>(),
            new List<char>(),
            new List<char>(),
            new List<char>(),
            new List<char>()
        };

        var validCharacters = GetValidCharacters();

        for (var i = 0; i < buttons.Length; i++)
        {
            var button = buttons[i];
            var column = i % 5;

            if (button.Character is null)
            {
                continue;
            }

            switch (button.State)
            {
                case ButtonState.State.InvalidLetter:
                    if (!validCharacters.Contains(button.Character!.Value))
                    {
                        for (var j = 0; j < invalidCharacters.Length; j++)
                        {
                            invalidCharacters[j].Add(button.Character!.Value);
                        }
                    }
                    break;
                case ButtonState.State.ValidLetter:
                    invalidCharacters[column].Add(button.Character!.Value);
                    break;
            }
        }

        return invalidCharacters;
    }

    private List<char> GetValidCharacters()
    {
        var validCharacters = new List<char>();

        for (var i = 0; i < buttons.Length; i++)
        {
            var button = buttons[i];
            switch (button.State)
            {
                case ButtonState.State.ValidLetter:
                case ButtonState.State.CorrectLetter:
                    validCharacters.Add(button.Character!.Value);
                    break;
            }
        }

        return validCharacters;
    }

    private char?[] GetCorrectCharacters()
    {
        var correctCharacters = new char?[5];

        for (var i = 0; i < buttons.Length; i++)
        {
            var button = buttons[i];
            var column = i % 5;

            switch (button.State)
            {
                case ButtonState.State.CorrectLetter:
                    correctCharacters[column] = button.Character!.Value;
                    break;
            }
        }

        return correctCharacters;
    }

    private BoardState GetBoardState()
    {
        var charactersUsed = new char[buttons.Length];
        var buttonStates = new ButtonState.State[buttons.Length];

        for (var i = 0; i < buttons.Length; i++)
        {
            charactersUsed[i] = buttons[i].Character ?? '\0';
            buttonStates[i] = buttons[i].State;
        }

        return new BoardState(charactersUsed, buttonStates);
    }
}
