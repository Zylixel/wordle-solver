﻿using WordleSolverWPF.Board;

namespace WordleSolverWPF.Cache;

internal static class BestGuessCache
{
    private static readonly Dictionary<BoardState, KeyValuePair<string, int>> bestGuesses = new();

    internal static bool TryGetGuess(in BoardState boardState, out KeyValuePair<string, int> bestGuess) => bestGuesses.TryGetValue(boardState, out bestGuess!);

    internal static void CacheGuess(in BoardState boardState, KeyValuePair<string, int> guess) => bestGuesses[boardState] = guess;
}
