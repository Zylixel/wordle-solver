using System.Collections.Immutable;
using System.IO;
using System.Reflection;

namespace WordleSolver.DataProvider;

internal static class WordListProvider
{
    private static ImmutableArray<string>? validGuessesCache = null;

    private static ImmutableArray<string>? answerCache = null;

    internal static ImmutableArray<string> GetValidGuesses()
    {
        if (validGuessesCache is not null)
        {
            return validGuessesCache.Value;
        }

        var validWords = new List<string>();

        using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WordleSolverWPF.Resources.ValidWords.txt"))
        {
            if (stream is null)
            {
                return ImmutableArray<string>.Empty;
            }

            using var reader = new StreamReader(stream);

            var line = reader.ReadLine();

            while (line is not null)
            {
                validWords.Add(line);
                line = reader.ReadLine();
            }
        }

        validGuessesCache = ImmutableArray.CreateRange(validWords);

        return validGuessesCache.Value;
    }

    internal static ImmutableArray<string> GetAnswers()
    {
        if (answerCache is not null)
        {
            return answerCache.Value;
        }

        var answers = new List<string>();

        using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WordleSolverWPF.Resources.ValidAnswers.txt"))
        {
            if (stream is null)
            {
                return ImmutableArray<string>.Empty;
            }

            using var reader = new StreamReader(stream);

            var line = reader.ReadLine();

            while (line is not null)
            {
                answers.Add(line);
                line = reader.ReadLine();
            }
        }

        answerCache = ImmutableArray.CreateRange(answers);

        return answerCache.Value;
    }
}