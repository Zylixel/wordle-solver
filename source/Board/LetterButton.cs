﻿using System.Windows;
using System.Windows.Controls;

namespace WordleSolverWPF.Board;

internal partial class LetterButton
{
    internal event EventHandler Updated;
    internal bool ShowWinningAnimation = false;

    private static readonly Random Random = new();

    private readonly int id = Random.Next();
    private readonly Button button;
    private char? character;
    private ButtonState.State state = ButtonState.State.NoLetter;

    public LetterButton(Button button, bool clickable)
    {
        this.button = button;
        button.Content = string.Empty;

        if (clickable)
        {
            button.Click += OnClick;
        }

        Updated += (_, _) => UpdateImage();
        Updated.Invoke(this, EventArgs.Empty);

    }

    internal char? Character
    {
        get => character;
        set
        {
            if (character == value)
            {
                return;
            }

            if (!value.HasValue)
            {
                state = ButtonState.State.NoLetter;
                button.Content = string.Empty;
                character = value;
            }
            else
            {
                state = ButtonState.State.InvalidLetter;
                button.Content = char.ToUpperInvariant(value.Value);
                character = char.ToLowerInvariant(value.Value);
            }

            Updated.Invoke(this, EventArgs.Empty);
        }
    }

    internal ButtonState.State State
    {
        get => state;
        set
        {
            if (Character is null && value != ButtonState.State.NoLetter)
            {
                return;
            }

            if (state != value)
            {
                state = value;
                Updated.Invoke(this, EventArgs.Empty);
            }
        }
    }

    internal void FakeClick() => OnClick(this, new RoutedEventArgs());

    private void OnClick(object sender, RoutedEventArgs e)
    {
        switch (State)
        {
            case ButtonState.State.NoLetter:
            case ButtonState.State.Preview:
            case ButtonState.State.PreviewCorrect:
                break;
            case ButtonState.State.InvalidLetter:
                State = ButtonState.State.ValidLetter;
                break;
            case ButtonState.State.ValidLetter:
                State = ButtonState.State.CorrectLetter;
                break;
            case ButtonState.State.CorrectLetter:
                State = ButtonState.State.InvalidLetter;
                break;
        }
    }

    private void UpdateImage()
    {
        button.Opacity = State.Opacity();
        button.Background = State.Brush();

        if (!ShowWinningAnimation)
        {
            button.Width = 70;
            button.Height = 70;
        }
    }

    internal void Render()
    {
        if (ShowWinningAnimation)
        {
            var time = TimeOnly.FromDateTime(DateTime.Now).Add(TimeSpan.FromMilliseconds(id));
            var offset = Math.Sin(time.Ticks * 0.0000005f) * 5;

            button.Width = 64 + offset;
            button.Height = 64 + offset;
            return;
        }
        else
        {
            button.Width = Math.Max(64, button.Width - 0.5f);
            button.Height = Math.Max(64, button.Height - 0.5f);
        }
    }
}
