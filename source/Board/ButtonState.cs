﻿using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WordleSolverWPF.Board;

internal static class ButtonState
{
    private static readonly Brush noLetterBrush;
    private static readonly Brush validLetterBrush;
    private static readonly Brush correctLetterBrush;

    static ButtonState()
    {
        var bmi = new BitmapImage(new Uri("pack://application:,,,/Resources/noLetterBackground.png"));
        noLetterBrush = new ImageBrush(bmi);

        bmi = new BitmapImage(new Uri("pack://application:,,,/Resources/ValidLetterBackground.png"));
        validLetterBrush = new ImageBrush(bmi);

        bmi = new BitmapImage(new Uri("pack://application:,,,/Resources/CorrectLetterBackground.png"));
        correctLetterBrush = new ImageBrush(bmi);
    }

    internal enum State : byte
    {
        NoLetter,
        InvalidLetter,
        ValidLetter,
        CorrectLetter,
        Preview,
        PreviewCorrect
    }

    internal static float Opacity(this State type) => type switch
    {
        State.PreviewCorrect or State.Preview or State.NoLetter => 0.5f,
        _ => 1f,
    };

    internal static Brush Brush(this State type) => type switch
    {
        State.PreviewCorrect or State.CorrectLetter => correctLetterBrush,
        State.ValidLetter => validLetterBrush,
        _ => noLetterBrush,
    };

    internal static bool Preview(this State type) => type == State.Preview || type == State.PreviewCorrect;
}
