﻿namespace WordleSolverWPF.Board;

internal readonly struct BoardState : IEquatable<BoardState>
{
    private readonly char[] boardCharacters;
    private readonly ButtonState.State[] boardStates;

    public BoardState(char[] boardCharacters, ButtonState.State[] boardStates)
    {
        this.boardCharacters = boardCharacters;
        this.boardStates = boardStates;
    }

    public override bool Equals(object? obj) => obj is BoardState state && Equals(state);

    public bool Equals(BoardState other)
    {
        if (boardCharacters.Length != other.boardCharacters.Length || boardStates.Length != other.boardStates.Length)
        {
            return false;
        }

        for (var i = 0; i < boardCharacters.Length; i++)
        {
            if (boardCharacters[i] != other.boardCharacters[i])
            {
                return false;
            }
        }

        for (var i = 0; i < boardStates.Length; i++)
        {
            if (boardStates[i] != other.boardStates[i])
            {
                return false;
            }
        }

        return true;
    }

    public static bool operator ==(BoardState left, BoardState right) => Equals(left, right);

    public static bool operator !=(BoardState left, BoardState right) => !Equals(left, right);

    public override int GetHashCode()
    {
        var hash = 0;

        for (var i = 0; i < boardCharacters.Length; i++)
        {
            hash = HashCode.Combine(hash, boardCharacters[i]);
        }

        for (var i = 0; i < boardStates.Length; i++)
        {
            hash = HashCode.Combine(hash, boardStates[i]);
        }

        return hash;
    }
}
