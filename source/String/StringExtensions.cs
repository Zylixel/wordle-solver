﻿namespace WordleSolverWPF.String;

internal static class StringExtensions
{
    internal static int DuplicateCount(this string input)
    {
        var counts = new char[26];

        for (var i = 0; i < input.Length; i++)
        {
            counts[input[i] - 97]++;
        }

        var sum = 0;

        for (var i = 0; i < counts.Length; i++)
        {
            var count = counts[i];

            if (count > 1)
            {
                sum += count - 1;
            }
        }

        return sum;
    }

    internal static bool ContainsInvalidCharacters(this string word, IList<char>[] invalidCharacters)
    {
        for (var i = 0; i < word.Length; i++)
        {
            var invalidCharactersInColumn = invalidCharacters[i];
            for (var j = 0; j < invalidCharactersInColumn.Count; j++)
            {
                if (invalidCharactersInColumn[j] == word[i])
                {
                    return true;
                }
            }
        }

        return false;
    }

    internal static bool ContainsAnyCharacters(this string word, IList<char> invalidCharacters)
    {
        for (var i = 0; i < invalidCharacters.Count; i++)
        {
            var character = invalidCharacters[i];

            if (word.Any(x => x.Equals(character)))
            {
                return true;
            }
        }

        return false;
    }

    internal static bool ContainsCorrectCharacters(this string word, char?[] correctCharacters)
    {
        for (var j = 0; j < correctCharacters.Length; j++)
        {
            var correctCharacter = correctCharacters[j];

            if (correctCharacter is not null)
            {
                if (word[j] != correctCharacter.Value)
                {
                    return false;
                }
            }
        }

        return true;
    }

    internal static bool ContainsAllCharacters(this string word, IList<char> characters)
    {
        for (var i = 0; i < characters.Count; i++)
        {
            if (!word.Contains(characters[i]))
            {
                return false;
            }
        }

        return true;
    }
}
