# Wordle Solver

A Wordle Solver written in .NET 6 with the Windows Presentation Foundation framework.

Solve (nearly) any problem on [Worlde](https://www.nytimes.com/games/wordle/index.html)!

Works on different websites, but is not as accurate due to the wordlists differing.

## Usage
Press `Enter` to fill in the suggested guess, or type to fill in your own guesses.

Click the letters or use the number keys to change the state of the current guess.

Press `Esc` to clear all guesses except for the first one.

## Visuals
![image](/uploads/b59db0991d447b2af341fceb07894ecb/image.png)
![image](/uploads/8b26f3ccbf98a624be9c2a5157ac3c62/image.png)
![image](/uploads/6c869e069c947fce341499d7bd92aced/image.png)

## Build
Recommended Build Parameters:
```dotnet build ./WordleSolverWPF.csproj --no-self-contained -a x64 --configuration [debug,release]```

## Authors and Acknowledgment
Project C# and XAML was written by Mason Myers.

Resources and Assets were pulled from [Wordle Game](https://wordlegame.org) to match styling.

Wordlist was pulled from https://github.com/tabatkins/wordle-list.

## Contributing
The main branch is not open to contributions.

## License
Licensed under the GNU General Public License v3.0

## Project status
This project is no longer in development. There are improvements that can be made to the algorithm, but I do not have the time to implement them.
